package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    // I hope without my comments you will understand how it works,
    // but if you need them don't hesitate to contact me

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        checkCorrectness(x,y);

        System.out.println(x);
        System.out.println(y);
        int j=0, size=0;
        label:
        for( int i=0; i<x.size(); i++ )
            for( ; j<y.size(); j++ )
                if( x.get(i).equals(y.get(j)) ){
                    size++;
                    System.out.print(y.get(j));
                    continue label;
                }
        if( size == x.size() ) return true;
        return false;
    }

    private void checkCorrectness(List x, List y){
        if( x == null || y == null ) throw new IllegalArgumentException();
    }
}
