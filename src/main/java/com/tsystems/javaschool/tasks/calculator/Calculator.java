package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if( isInputStatementCorrect(statement) ) return null;
        return solve(statement);
    }

    private boolean isInputStatementCorrect(String statement){
        return  statement==null ||
                statement.equals("") ||
                statement.contains(",");    // why do I need this? my program perfectly works with this
    }

    // this method goes from inner braces to outer braces
    public String solve(String equation){
        List<Integer> startBraces = getAllIndexesOfCharInString("\\(", equation);
        List<Integer> endBraces = getAllIndexesOfCharInString("\\)", equation);

        // if there is no braces, equation is simple, so we can just solve it
        if( startBraces.size() == 0 && endBraces.size() == 0 ) return solveSimpleEquation(equation);

        if( !isBracesConditionsCorrect(equation, startBraces, endBraces) ) return null;

        // this algorithm searched for first start brace (position in startBrace is j),
        // which position will be righter, than first end brace,
        // and took everything between startBrace[j-1] and endBrace[0]

        // so... if we don't have start brace after first end brace, it won't work,
        // because of this we place start brace on endBrace[end].position+1 (follow line)
        startBraces.add(endBraces.get(endBraces.size()-1)+1);
        for(int i=0; i<endBraces.size();) {
            for (int j = 0; j < startBraces.size(); j++) {
                if (endBraces.get(i) < startBraces.get(j)) {

                    divide(equation, startBraces.get(j - 1), endBraces.get(i));
                    String before = this.before;
                    String after = this.after;

                    String withoutOnePairOfBraces = solve( between );

                    // this means, that we throw all braces away
                    if(before.equals("") && after.equals("")) return withoutOnePairOfBraces;

                    return solve( before+withoutOnePairOfBraces+after );
                }
            }
        }
        return null;
    }

    private boolean isBracesConditionsCorrect(String equation, List<Integer> startBraces, List<Integer> endBraces){

        if( equation.contains("null") ) return false;

        return  startBraces != null && endBraces != null &&
//                startBraces.size() > 0 && endBraces.size() > 0 &&
                startBraces.size() == endBraces.size() &&
                endBraces.get( endBraces.size()-1 ) > startBraces.get( startBraces.size()-1 ) && // по идее тут нужно выбрасывать исключение
                endBraces.get( 0 ) > startBraces.get( 0 );

    }

    String before, between, after;
    private void divide(String s, int startIndex, int endIndex){
        before = s.substring(0, startIndex);
        after = s.substring(endIndex+1);
        between =  s.substring(startIndex+1, endIndex);
    }

    private List<Integer> getAllIndexesOfCharInString(String givenChar, String originString){
        List<Integer> indexes = new ArrayList<>();

        Matcher matcher = Pattern.compile( givenChar ).matcher(originString);
        while (matcher.find()){
            indexes.add(matcher.start());
        }
        return indexes;
    }



//------------------------ big logic block, that solve simple equations ----------------------------------

    public String solveSimpleEquation(String equation){

        MathActionsLists actionLists = new MathActionsLists( equation );

        if( !isSolvingEquationCorrect( actionLists ) ) return null;

        List<Double> numbers;
        try {
            numbers = parseDoubles(equation, actionLists);
        } catch ( Exception e ){
            return null;    // it will be much more better to throw here exception or print stack trace
        }

        // solving the equation: twice running through all math actions.
        // Firstly, multiplication and division are done.
        // Secondly, addition and subtraction are done.
        for(int i=0, index=0; i<actionLists.getAllActions().size(); i++, index++){
            if(actionLists.getMultiplication().contains(actionLists.getAllActions().get(i))) {
                index = makeMathAction( numbers,numbers.get(index) * numbers.get(index + 1), index );
            }
            if(actionLists.getDivision().contains(actionLists.getAllActions().get(i))) {
                index = makeMathAction( numbers,numbers.get(index) / numbers.get(index + 1), index );
            }
        }
        Double result = numbers.get(0);
        for(int i=0, counter=1; i<actionLists.getAllActions().size(); i++){
            if(actionLists.getAddition().contains(actionLists.getAllActions().get(i))) {
                result += numbers.get(counter++);
            }
            if(actionLists.getSubtraction().contains(actionLists.getAllActions().get(i))) {
                result -= numbers.get(counter++);
            }
        }

        if( !isResultCorrect(result) ) return null;

        return String.valueOf( floatToInt(result) );
    }

    private class MathActionsLists{

        private final List<Integer> addition;
        private final List<Integer> subtraction;
        private final List<Integer> multiplication;
        private final List<Integer> division;
        private final List<Integer> allActions = new ArrayList<>();

        public List<Integer> getAddition() {return addition;}
        public List<Integer> getSubtraction() {return subtraction;}
        public List<Integer> getMultiplication() {return multiplication;}
        public List<Integer> getDivision() {return division;}
        public List<Integer> getAllActions() {return allActions;}

        MathActionsLists(String equation){
            addition = getAllIndexesOfCharInString("\\+", equation);
            subtraction = getAllIndexesOfCharInString("-", equation);
            multiplication = getAllIndexesOfCharInString("\\*", equation);
            division = getAllIndexesOfCharInString("/", equation);

            allActions.addAll(addition);
            allActions.addAll(subtraction);
            allActions.addAll(multiplication);
            allActions.addAll(division);

            Collections.sort(allActions);
        }

    }

    private boolean isSolvingEquationCorrect( MathActionsLists actionLists ){
        // if two math operations near, include two subtractions, equations contains mistake
        for( int i=1; i<actionLists.getAllActions().size(); i++ ){
            if( actionLists.getAllActions().get(i-1)+1 == actionLists.getAllActions().get(i) ) {

                boolean isPreviousActionSubtraction = actionLists.getSubtraction().contains( actionLists.getAllActions().get(i-1) );
                boolean isThisActionSubtraction = actionLists.getSubtraction().contains( actionLists.getAllActions().get(i) );

                if(     (!isPreviousActionSubtraction && !isThisActionSubtraction) || // two math operations near
                        ( isPreviousActionSubtraction && isThisActionSubtraction ) )  // two subtractions near
                    return false;

                // but two operators near and last is subtraction,
                // means that last subtraction isn't math action, just next number is negative
                actionLists.getAllActions().remove(i);
            }
        }
        return true;
    }

    private List<Double> parseDoubles(String equation, MathActionsLists actionLists) throws Exception{
        List<Double> numbers = new LinkedList<>();

        numbers.add( Double.valueOf( equation.substring( 0, actionLists.getAllActions().get(0) ) ) );
        for ( int i=1; i<actionLists.getAllActions().size(); i++ ){
            numbers.add( Double.valueOf( equation.substring( actionLists.getAllActions().get(i-1)+1, actionLists.getAllActions().get(i) ) ) );
        }
        numbers.add( Double.valueOf( equation.substring( actionLists.getAllActions().get( actionLists.getAllActions().size()-1 )+1 ) ) );

        return numbers;
    }

    private int makeMathAction(List<Double> numbers, double number, int index){
        numbers.remove(index);
        numbers.set( index, number );
        return --index;
    }

    private boolean isResultCorrect(Double result){
        return !result.isInfinite() && !result.isNaN();
    }

    private Number floatToInt(double number){

        double remainder = number%1;
        double eps = 1e-8;

        if( remainder < eps && remainder > -eps )
            return Math.round(Math.round(number));

        return number;
    }
}
