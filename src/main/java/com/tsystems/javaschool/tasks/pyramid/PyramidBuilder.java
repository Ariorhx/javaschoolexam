package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    // I hope without my comments you will understand how it works,
    // but if you need them don't hesitate to contact me
    int colonLength, rowLength, expectedInputLength;

    /**
     * Builds a pyramid with sorted values (with minimum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        checkCorrectness(inputNumbers);

        Collections.sort(inputNumbers);
        int [][] output = createOutput();
        build(output, inputNumbers);

        return output;
    }

    private void checkCorrectness(List<Integer> input){
        if(     input.size() >= Integer.MAX_VALUE-1 ||
                input.contains(null)
        ) throw new CannotBuildPyramidException();

        getPyramidColonLength(input);
        if(expectedInputLength != input.size()) throw new CannotBuildPyramidException();
    }

    private void getPyramidColonLength(List<Integer> input){
        int counter = 2;
        for( expectedInputLength=1; expectedInputLength<input.size(); expectedInputLength+=counter++ ){}
        colonLength = counter-1;
    }

    private int[][] createOutput(){
        rowLength = 2* colonLength -1;
        return new int[colonLength][rowLength];
    }

    private void build(int[][] output, List<Integer> input){
        int center = Math.floorDiv(rowLength,2 );

        int counter = 0;
        for(int i = 0; i< colonLength; i++){
            int position = center -i;
            for(int j=0; j<i+1; j++){
                output[i][position] = input.get(counter++);
                position += 2;
            }
        }
    }

}
